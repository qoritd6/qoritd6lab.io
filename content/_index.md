---
title: Star Wars D6
---

# Star Wars D6

This is an introduction to the usage of Star Wars D6 as a roll system for the
guild. It specifically aims to cover character creation, usage, and custom
rules. It also covers clarification and repetition of rules that are likely to
be important.

You can find the PDF for Star Wars D6 [here](/swd6.pdf). You *will* need this
PDF to look up certain rules, although this document attempts to cover the
basics as concisely as possible.

The main draw of Star Wars D6 is that it is a relatively simple system that is
tailor-fit for "a galaxy far, far away". Unfortunately, there are a few things
inside the system that make it less fit for online use, usually because it
sacrifices expediency for detail. This document proposes a few changes to keep
things moving.

The use of the system comes with a plea to Keep Things Simple. While it is
certainly possible to do advanced things with the system, and it is sometimes
good to do advanced things, it might often be more desireable to just do the
simple thing.

The source code for this website can be found at
<https://gitlab.com/qoritd6/qoritd6.gitlab.io/>. The relevant files are in
`content/`. You can submit issues and propose changes via the web interface. If
you get lost in the web interface (which you very well might), simply download
the file, edit it locally, and send it to Ffwdwr as a proposal.
