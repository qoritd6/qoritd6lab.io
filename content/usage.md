---
title: Usage
---

The core of the D6 system is extremely simple: You roll the dice that it says
next to your skill or attribute, and the GM determines whether you succeeded or
not by comparing your roll to the difficulty that the GM had decided on.

**If a character's skill roll is equal to or higher than the difficulty, they
succeed.**

Use the below table to determine difficulty:

|   Difficulty   | Difficulty Numbers | Average |
| :------------: | :----------------: | :-----: |
|   Very Easy    |        1-5         |    3    |
|      Easy      |        6-10        |    8    |
|    Moderate    |       11-15        |   13    |
|   Difficult    |       16-20        |   18    |
| Very Difficult |       21-30        |   25    |
|     Heroic     |        31+         |    —    |

Example:

> Heria wants to disable the alarm system of a Republic military facility. The
> DM asks her to roll Security to see if it works. She has a Security of 6D6+2,
> and rolls a total of 24+2. This is well above the Moderate difficulty of 13
> determined by the GM, and so she succeeds.

**Important:** When a character uses one of the prerequisite skills of an
advanced skill, add the advanced skill to the prerequisite skill's roll. For
instance, if you have 4D in First Aid and 1D in (A) Medicine, you roll 4D + 1D
(5D) for all First Aid rolls. The reverse is not true.

## Combat

Combat is explained on [this page](/combat).
