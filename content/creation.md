---
title: Character creation
---

Character sheets in Star Wars D6 are remarkably simple, but a little bit of
complexity goes into creating them. Examples go a long way to making this
process as simple as possible. Find [here](/sheet.svg) a filled-in example
sheet.

Going through the sheet:

- Name, gender, age, height, weight, and physical description all speak for
  themselves.

- Species determines a couple of variables that will be obvious later.

- Type is your own description of the "class" of your character. It can be
  anything.

- Dexterity, Knowledge, Mechanical, Perception, Strength, and Technical are your
  **attributes**. The "3D+1" score next to Technology means that you may roll
  three six-sided dice and add one to the result to determine what you have
  rolled (3d6+1).

- The items listed below the attributes are your **skills**. They are
  narrowed-down versions of the attributes. If no value is listed beside the
  skill, you roll the same as your attribute dice. If a value is listed beside
  the skill, you roll that instead.

- The Move value determines how many metres you can walk normally in a single
  round. More on this later.

- The rest of the sheet is unimportant.

## Acquiring attributes and skills

Acquiring attributes is a simple matter. You use a system of point buy to put
dice into each attribute. Your species gives you a certain amount of attribute
dice (usually 12D), and you gain an additional 6D to spend. When spending dice,
you must remain between the minimum and maximum determined by your species. For
instance, a twi'lek must put at least 2 dice into Dexterity, and can put no more
than 4 dice into Dexterity.

If you believe that you should have more than the maximum in your attribute dice
(or less than the minimum), ping an officer. It's probably possible.

Acquiring skills works a little differently. In the understanding that all
characters are different and inherently out of balance, and usually don't start
at "level 1", we use a system of simply guessing. You take a look at the below
chart, and assign dice to each relevant skill as befits how skilful you imagine
your character is.

| Dice  |                                                      Skill level                                                      |
| :---: | :-------------------------------------------------------------------------------------------------------------------: |
|  1D   |                                         Below human average for an attribute.                                         |
|  2D   |                                    Human average for an attribute and many skills.                                    |
|  3D   |                                        Average level of training for a human.                                         |
|  4D   |                                      Professional level of training for a human.                                      |
|  5D   |                                               Above average expertise.                                                |
|  6D   | Considered about the best in a city or a geographic area. 1 in 100,000 people will have training to this skill level. |
|  7D   |        Among the best on the continent. About one in 10,000,000 people will have training to this skill level.        |
|  8D   |           Among the best on a planet. About 1 in 100,000,000 people will have training to this skill level.           |
|  9D   |  One of the best of several systems in the immediate area. About one in a billion people have a skill at this level.  |
|  10D  |                                             One of the best in a sector.                                              |
|  12D  |                                             One of the best in a region.                                              |
| 14D+  |                                            One of the best in the galaxy.                                             |

As a general guide, you should probably be cautious to put 6D or above and
hesitant to put 8D or above in any skills. When you're done, you show the sheet
to an officer to get it okayed. If, after a few events, you get the feeling that
you messed up a couple of dice here and there, get in touch with an officer
again and it can probably be fixed.

Page 34 has a list of all possible skills. Skills are generally fairly specific,
and as such there are a lot of them.

A few notes:

- Skills always start at the attribute baseline. For instance, if your Dexterity
  attribute is 4D, then your Blaster skill is necessarily 4D or higher.

- If you estimate that your skill is somewhere between---for example---4D and
  5D, you can instead use 4D+1 or 4D+2. 4D+3 is not possible, and would be
  equivalent to 5D. See page 23 for a description of how "pips" work.

- Specialisations (page 21) are easier to have many dice in.

- Advanced skills (page 22) are more difficult to have many dice in. Moreover,
  advanced skills start at 0D, not your attribute baseline. So you may have 1D
  in (A) Medicine, even though your Technical is 4D.

You can use [this
spreadsheet](https://docs.google.com/spreadsheets/d/1uKesFlo3mvszy9H7nk-07pTKVeGJAg0avrs7mMo3urA/edit#gid=0)
to guide you along.

## Species

Your species determines the following things for you:

- How many attribute dice you start out with.
- What your minimum and maximum dice are in certain attributes.
- Any special abilities.
- Some story fluff.
- Your Move speed.

You can find the stat block for your species in the rules book. If your species
is not in there, it is probably in [this pdf](/aliens.pdf). If your species
cannot be found in either source, ping an officer.

Find below the entry for twi'leks. It should be fairly self-explanatory. All
numbers on the left side of the divide are minimums, and all numbers on the
right side are maximums.

> #### Twi'lek
>
> **Attribute Dice:** 12D\
> **DEXTERITY 2D/4D+1**\
> **KNOWLEDGE 1D/4D**\
> **MECHANICAL 1D/3D**\
> **PERCEPTION 2D/4D+2**\
> **STRENGTH 1D/3D+2**\
> **TECHNICAL 1D/3D**\
> **Special Abilities:**\
> *Tentacles*: Twi'leks can use their tentacles to communicate in secret
> with each other, even if in a room full of individuals. The complex
> movement of the tentacles is, in a sense, a "secret" language that
> all Twi'leks are fluent in.\
> *Skill Bonus*: At the time the character is generated only, the
> character receives 2D for every 1D placed in the persuasion skill.\
> **Story Factors:**\
> *Slavery*: Slavery is so ingrained as the main trade of Ryloth, that most
> Twi'leks are generally thought to be either a slave or consort of
> some kind, and often treated as second class citizens, this is espe­cially true
> in Hutt space.\
> **Move:** 10 / 12\
> **Size:** 1.6-2.0 meters tall

## Equipment

Equipment is explained on [this page](/equipment).

## Progression

Some people's characters get better over time. Other people's characters have
reached their apex and generally stay around the same level of aptitude. Some
people prefer to progress slowly, and others don't have all the time in the
world to see the progression arc of their character.

With all these different stakes, it's a little difficult to etch out a proper
progression system. Star Wars D6 offers a decent foundation for progression,
however, if a little gamey. Below are two systems of progression that you can
use to advance your character. The two systems do not mix well; consult an
officer when jumping back and forth.

### Character points

After every "adventure", you are awarded character points that you can keep
track of on your character sheet. An adventure is *usually* an event, but can
also be any other kind of RP where dice are rolled.

You start with zero character points.

Consult the below table for how many character points are awarded:

|      Example       | Character Points |
| :----------------: | :--------------: |
| A training session |        3         |
|    A tiny event    |        6         |
|  A regular event   |        9         |

You can use these character points to advance your character as per the rules on
page 25 onwards. Training time is a suggestion.

### Just wing it

You may have a good idea for how well and how expediently your character
advances. If they came out of an event where they swindled everybody into
believing that they are a high Republic official, then it might make sense to
bump up their Con by a pip. If your character recently successfully resisted
interrogation, then maybe increase their Willpower by one pip.

This is truly a system of winging it, and it might make sense to poke an officer
every so often to see if things still make sense.