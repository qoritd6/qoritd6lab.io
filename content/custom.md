---
title: Custom rules
---

## Scrapped rules

The rules book list a lot of rules. To keep things simple, these rules are
scrapped:

- (Chapter 2) Character creation is done through a point buy for attributes, and
  custom assignment for skill dice. This chapter can largely be ignored.

- (Chapter 2) Advantages and disadvantages are not used.

- (Chapter 5) No wild (exploding) dice.

- (Chapter 5) No dice simplification; all dice are always rolled.

- (Chapter 5) Character Points are only optionally used for progression.

- (Chapter 5) No Force Points.

- (Chapter 6) Drawing a weapon and setting it to stun are both free actions.

- (Chapter 6) Ammunition is not tracked.

- (Chapter 6) Mechanics for protection (e.g., the table behind which you are
  hiding taking damage) are not used.

- (Chapter 6) Weapons and armour damage is optional.

## Changed rules

- (Chapter 5) You can only take one action per round, plus reactions, plus
  movement. This is concisely described in [Combat](/combat). This keeps things
  expedient. If you really want to do multiple actions in a round and you
  believe the circumstances are exceptional, look up the rules and apply them.

- (Chapter 5) Initiative is changed to be more D&D-like, see [Combat](/combat).

- (Chapter 6) Grenade deviation described on page 92 is randomised by the GM.

- (Chapter 6) Partial cover increases the difficulty by a flat 7, see
  [Combat](/combat).

<!-- - (Chapter 6/7) TODO Movement/ranges -->

## Rule clarifications

- (Chapter 8) TODO Space combat.

- (Chapter 10) TODO The Force.

- (Chapter 19) Droid characters and droid engineering is handled on a
  case-by-case basis.