---
title: Combat
---

Combat works similar to the core mechanic, but adds a couple of complexities on
top of it. Find below the way combat works as quickly as possible.

## Initiative

When combat begins, every character rolls Perception to determine the order in
which they act. Characters act from highest roll to lowest roll. If two people
roll the same number, the GM randomly decides who begins. Once the last person
has acted, a new round starts again at the top.

## Move

Page 107 contains the gritty details of how movement works, but it boils down to
this:

- Your movement is determined by your species, usually around 10 (metres).

- You can do *cautious movement* (slow-walking) at half your Move speed as a
  free action.

- You can do *cruising movement* (walking) at your Move speed as an action. If
  you also want to do something else during your turn, you have a stacking -1D
  penalty to all rolls during the rest of the round.

- *High speed* movement (2x) and *all-out* movement (4x) are also
  available, but you'll have to look up the rules for these yourself.

## Make an attack

There are a couple of actions you could be doing during combat, but making an
attack is the most likely course of action. To make an attack, roll the skill
that is appropriate for your weapon (e.g., Blaster for a blaster pistol, Melee
Combat for a vibroblade).

## Determine the difficulty

Whether or not you hit is determined by the same difficulty mechanic as regular
checks. The difficulty scales differently depending on the weapon you're using.

For ranged weapons, the difficulty is determined by your distance to the target.
Find below an example table for the blaster pistol. Please note that the metres
in this example are *not applicable to other weapons*. The rest is, however.

| Difficulty |    Range    |     Metres     |
| :--------: | :---------: | :------------: |
| Very Easy  | Point-blank | Under 3 metres |
|    Easy    |    Short    |  3-10 metres   |
|  Moderate  |   Medium    |  11-30 metres  |
| Difficult  |    Long     | 31-120 metres  |

For melee weapons, the difficulty is inherent to the weapon. For instance, to
hit with a vibroblade, you must always succeed a Moderate difficulty.

Being behind partial cover increases the difficulty by 7.

The GM can modify the difficulty to their heart's content.

### Defend

If struck by an attack, the recipient may opt to defend themselves. The skill
used to defend oneself is usually Dodge for ranged attacks, and Melee Parry or
Brawling Parry for melee attacks.

By rolling to defend oneself, the result of the roll becomes the new difficulty
score that *all attacks of that type* should meet for the rest of the round.

By using a reaction, all your rolls during the rest of the round have a stacking
-1D penalty.

## Deal damage

Your ranged weapon does a certain amount of damage. A blaster deals 4D damage,
for instance.

A melee weapon might have a damage code of STR+1D. That means, the attacker
rolls his Strength and adds one extra die for damage. (If there's a maximum
listed—such as "maximum 6D"—that's the maximum damage for the weapon regardless
of the user's Strength.)

To keep things expedient, **always roll damage immediately after making an
attack**, regardless of whether you hit or not. This keeps things moving at a
much quicker pace.

There are certain scenarios in which a weapon's damage may be modified, but that
is outside of the scope of this guide.

## Resist damage

When struck, you roll Strength to resist the damage. Armour may modify this
roll. For instance, blast armor adds +1D to your damage resistance.

If the resistance roll is higher than the damage roll, the attack has no effect.

If the damage roll is higher than the resistance roll, the attack has an effect
as listed in the following table:

| Damage Roll ≥ Strength Roll By: |      Effect      |                                           Summary                                           |
| :-----------------------------: | :--------------: | :-----------------------------------------------------------------------------------------: |
|               1-3               |     Stunned      |                  -1D to rolls for the rest of the round and the next round                  |
|               4-8               |     Wounded      | -1D/-2D to rolls until healed; fall prone and can take no actions for the rest of the round |
|              9-12               |  Incapacitated   |                     Fall prone and knocked unconscious for 10D minutes                      |
|              13-15              | Mortally Wounded |              Fall prone and knocked unconscious; roll 2D every turn to survive              |
|               16+               |      Killed      |                                            Dead                                             |

Weapons set for stun roll damage normally, but treat any result more serious
than "stunned" as "unconscious for 2D minutes." Unless specifically stated
otherwise, all character-scale blasters can be set for stun damage.

### Injuries

**Stunned** characters suffer a penalty of −1D to skill and attribute rolls for
the rest of the round and for the next round. A stun no longer penalizes a
character after the second round, but it is still "affecting" him for half an
hour unless the character rests for one minute.

If a character is being "affected" from a number of stuns equal to the number
before the "D" for the charac­ ter's Strength, the character is knocked
unconscious for 2D minutes. A character making an Easy first aid total can
revive an unconscious character.

**Wounded** characters fall prone and can take no actions for the rest of the
round. The character suffers a penalty of −1D to skill and attribute rolls until
he heals (through medpacs or natural rest). A character who is wounded a second
time is wounded twice.

A character who's wounded twice falls prone and can take no actions for the rest
of the round. The character suffers a penalty of −2D to all skill and attribute
rolls until he is healed. A wounded twice character who is wounded again is
incapacitated.

An **incapacitated** character falls prone and is knocked unconscious for 10D
minutes. The character can't do any­thing until healed. An incapacitated
character who is wounded or incapacitated again becomes mortally wounded.

A character making a Moderate first aid total can revive an incapacitated
character. The incapacitated char­acter is now awake, but is groggy, cannot use
skills, and can only move at half his "cautious" rate. (See chapter 7.)

A **mortally wounded** character falls prone and is unconscious. The character
can't do anything until healed. The character may die — at the end of each
round, roll 2D. If the roll is less than the number of rounds that the
char­acter has been mortally wounded, the character dies. A mortally wounded
character who is incapacitated or mor­tally wounded again is killed.

A character making a Moderate first aid total can "sta­bilise" a mortally
wounded character. The character is still mortally wounded but will survive if a
medpac or kolto tank is used on him within one hour (Moderate first aid total);
otherwise, he dies. (This is different from healing a character with a medpac;
see "Healing".)

A **killed** character is... killed. Start rolling up a new character. Character
death cannot be forced upon you, however, so you may also consider this a
grievous injury.

## Healing

Page 102 has the full details on healing. The important takeaway is that there
are the following methods of healing:

- Natural healing over longer periods of time.
- Medpacs as first aid.
- Kolto tanks over the span of hours.
- Surgery when the injury is not curable by kolto tanks.
