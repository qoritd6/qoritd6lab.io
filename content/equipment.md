---
title: Equipment
---

Equipment is a fiddly thing. This section is written under the supposition that
most people do not want to track every little item that is on their person, and
don't necessarily want rules for every single item. However, knowing what kind
of items are on your character is a handy thing to know in advance, lest you
stand in front of a perimeter wall and aren't sure whether your character would
realistically have rope and a grappling hook with them.

Unfortunately, the D6 system is fairly granular in its equipment. The [core
rules](/swd6.pdf) contains a big list of equipment. To make things easier on
everyone, and with the understanding that players know the wealth level of their
own characters, you can pick any items from the book.

You should strive to have **no more than 20 items**. This is a soft cap that
reduces analysis paralysis.

[This website](https://rancorpit.com/forums/downloads/index.html) is a fan
resource that, among other things, provides compilations of all available
equipment. If you want, you can go through the compilations for an item that is
not available in other resources. Please consult with an officer before actually
adding the item to your sheet, however.

This section contains a list of common, mundane items to inspire you, and a list
of custom items that are missing from the books.

## Mundane items

Find here a list of items for your inspiration. Some of these items can also be
found in the source books. If a mundane item is not listed here, feel free to
add it regardless. If your character has signature bubblegum, they can bring
that with them.

|        Examples         |            Skill             |   Cost    | Availability |
| :---------------------: | :--------------------------: | :-------: | :----------: |
|        Bed roll         |              —               |    50     |      1       |
|      Binder cuffs       |              —               |    50     |      1       |
|       Breath mask       |              —               |    200    |      1       |
|      Code cylinder      | Computer programming/repair  |    500    |      2       |
|    Comlink, personal    |        Communications        |    25     |      1       |
|    Comlink, military    |        Communications        |    100    |     2, R     |
|   Computer, portable    | Computer programming/repair  | 500-1000  |      1       |
|        Data card        | Computer programming/repair  |    10     |      1       |
|         Datapad         | Computer programming/repair  | 100-1000  |      1       |
|       Energy cell       |              —               |    10     |      1       |
|     Food and drink      |              —               |     —     |      1       |
|        Glow rod         |              —               |    10     |      1       |
|        Ion flare        |              —               |    20     |      1       |
|    Listening device     |              —               |    750    |     2, R     |
|     Macrobinucolars     |           Sensors            |    100    |      1       |
|         Medpac          |          First aid           |    100    |      1       |
|   Musical instrument    | Musical instrument operation |     —     |      —       |
|   Playing cards, dice   |           Gambling           |     —     |      1       |
|    Pocket scrambler     |        Communications        |    400    |  4, F or R   |
|      Recording rod      |              —               |  25-100   |      1       |
| Replacement prosthetics |              —               | 1000-5000 |      4       |
| Rope and grappling hook |              —               |  50-150   |      1       |
|      Security kit       |           Security           | 750-1500  |  2, R or X   |
|  Sensor pack, portable  |           Sensors            |    300    |      2       |
|      Shock collar       |              —               |   1000    |     3, X     |
|       Space suit        |              —               |   2000    |     2, F     |
|  Speeder, speeder bike  |   Ground vehicle operation   |     —     |      —       |
|   Spice, recreational   |              —               |     —     |  2, R or X   |
|   Tool kit, enhanced    |           Various            | 500-1000  |      1       |
|   Tool kit, standard    |           Various            |    650    |      1       |
|     Tracking device     |            Search            | 400-1000  |      2       |
|     Transliterator      |        Communications        |   1200    |      3       |
|       Vacuum suit       |              —               |   1000    |      1       |

## Custom items

### Holographic disguise matrix

**Type:** Holographic disguise system\
**Cost:** 20000\
**Availability:** 3, R or X\
**Game Notes:** The disguise matrix allows you to disguise yourself by
projecting a series of images capable of matching your movements. It takes a
Very Difficult *search* roll to detect the illusion, though sensors, cameras and
droids get a +2D bonus to detect it. An advanced model, for double the price,
includes sensor nodes that track a number of other factors, such as ambient
temperature and weather conditions, making the image react to those variables.
This increases the search diffi­culty to Heroic, and lowers to +1D the bonus to
electronic examin­ers.

### Stealth generator

**Type:** Personal cloaking device\
**Skill:** Sneak\
**Cost:** 1500\
**Availability:** 2, R\
**Game Notes:** You become invisible. You receive a +3D bonus when using the
*sneak* skill.
